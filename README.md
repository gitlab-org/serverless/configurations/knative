# Knative Configurations

## Logging

### Enable Request Logs

Run the following command in order to enable request logs:

```
kubectl edit cm -n knative-serving config-observability
```

Copy the logging.request-log-template from the data._example field to the data field one level up in the hierarchy.

### Add Logging to Cluster

Run the following command in order to send logs to the Elasticsearch GitLab Managed App:

```
kubectl apply -f https://gitlab.com/gitlab-org/serverless/configurations/knative/raw/v0.7.0/monitoring-logs-elasticsearch.yaml
```

### Create Elasticsearch Index

To view logs with Kibana, you need to create an index in Elasticsearch.

Open the Kibana UI, and click on "Management", then "Index Patterns".

Ensure that a `logstash-*` index exists. If it does not, click "Create index pattern", and specify `logstash-*` for "Index pattern" and select `@timestamp` from the "Time Filter field name" dropdown, and click on the "Create" button.

### Viewing Request Logs

Open the Kibana UI, and click on "Discover", then select `logstash-*` from the dropdown on the left, and enter `_exists_:"httpRequest.requestUrl"` into the search box.
